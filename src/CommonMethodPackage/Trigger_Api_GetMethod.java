package CommonMethodPackage;
import static io.restassured.RestAssured.given;

import RequestRepositoryPackage.Get_RequestRepository;

import static io.restassured.RestAssured.given;

public class Trigger_Api_GetMethod extends Get_RequestRepository{

	public static int extract_status_code(String URL) {
		int Statuscode = given().when().get(URL).then().extract().statusCode();
		return Statuscode;
	}
	public static String extract_response_body(String URL) {
		String ResponseBody = given().when().get(URL).then().extract().response().asString();
		return ResponseBody;
}
	
	

}
