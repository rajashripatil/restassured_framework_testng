package CommonMethodPackage;

import static io.restassured.RestAssured.given;

import RequestRepositoryPackage.Put_RequestRepository;

public class Trigger_Api_PutMethod extends Put_RequestRepository{
	// create method to extract Status code

	public static int extract_status_code(String req_Body, String putURL) {

		int Statuscode = given().header("Content-Type", "application/json").body(req_Body).when().put(putURL).then()
				.extract().statusCode();

		return Statuscode;
	}
	// create method to extract response body

	public static String extract_responsebody(String req_Body, String URL) {

		String responseBody = given().header("Content-Type", "application/json").body(req_Body).when().put(URL).then()
				.extract().response().asString();
		return responseBody;

	}

}
